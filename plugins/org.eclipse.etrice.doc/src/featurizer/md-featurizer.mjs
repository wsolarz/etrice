// @ts-check

import fs from 'fs';
import unified from 'unified';
import remarkParse from 'remark-parse';
import remarkMdx from 'remark-mdx';
import mdastToString from 'mdast-util-to-string';

/**
 * @typedef {import('unist').Node} Node
 * @typedef {import('mdast').Parent} Parent
 * @typedef {import('mdast').Root} Root
 * @typedef {import('mdast').Content} RootContent
 * @typedef {import('mdast').Heading} Heading
 * @typedef {import('mdast').Table} Table
 */

/**
 * @typedef {Object} Feature
 * @property {string} name
 * @property {boolean} isPackage
 * @property {Array<Relation>} relations
 * 
 * @typedef {Object} Relation
 * @property {string} type
 * @property {string} feature
 */

/** @enum {string} */
const RelationType = {
	IsA: "Is a:",
	InheritingFeatures: "Inheriting features:",
	IsOfType: "Is of type:",
	Typecasts: "Typecasts:",
	Contains: "Contains:",
	IsContainedIn: "Is contained in:",
	Uses: "Uses:",
	IsUsedBy: "Is used by:",
	Views: "Views:",
	IsShownBy: "Is shown by:",
	Edits: "Edits:",
	IsEditedBy: "Is edited by:"
};

/**
 * @param {string} keyword 
 * @returns {RelationType|undefined}
 */
function typeFromKeyword(keyword) {
	return Object.values(RelationType).find(relationType => relationType === keyword);
}

/**
 * @param {RelationType} relationType 
 * @returns {RelationType} 
 */
function opposite(relationType) {
	switch(relationType) {
		case RelationType.IsA: return RelationType.InheritingFeatures;
		case RelationType.InheritingFeatures: return RelationType.IsA;
		case RelationType.IsOfType: return RelationType.Typecasts;
		case RelationType.Typecasts: return RelationType.IsOfType;
		case RelationType.Contains: return RelationType.IsContainedIn;
		case RelationType.IsContainedIn: return RelationType.Contains;
		case RelationType.Uses: return RelationType.IsUsedBy;
		case RelationType.IsUsedBy: return RelationType.Uses;
		case RelationType.Views: return RelationType.IsShownBy;
		case RelationType.IsShownBy: return RelationType.Views;
		case RelationType.Edits: return RelationType.IsEditedBy;
		case RelationType.IsEditedBy: return RelationType.Edits;
	}
	throw new Error('unknown relation type: ' + relationType);
}

export function checkReferenceDocs() {
	const basePath = './docs/reference/';
	const fileNames = fs.readdirSync(basePath)

	const features = fileNames.flatMap(fileName => {
		const buffer = fs.readFileSync(basePath + fileName);
		const tree = unified()
			.use(remarkParse)
			.use(remarkMdx)
			.parse(buffer);
		
		if(tree.type !== 'root') {
			console.error('Expected root node! (' + fileName +')');
			process.exitCode = 1;
			return [];
		}
		const root = /** @type {Root} */ (tree);
		return parseFeatures(root);
	});

	const errors = checkFeatures(features);
	if(errors.length > 0) {
		errors.forEach(error => console.error(error));
		process.exitCode = 1;
	} else {
		console.log('No errors found!');
	}
}

/**
 * @param {Root} root
 * @returns {Array<Feature>}
 */
export function parseFeatures(root) {
	const sections = splitSections(root);
	const features = sections.map(parseFeature);
	return features;
}

/**
 * @param {Root} root
 * @returns {Array<{ heading: Heading, content: Array<RootContent> }>}
 */
function splitSections(root) {
	/** @type {Array<{ heading: Heading, content: Array<RootContent> }>} */
	const result = [];
	for(const child of root.children) {
		if(child.type === 'heading') {
			const heading = /** @type {Heading} */ (child);
			result.push({ heading: heading, content: [] });
		} else {
			result.at(-1)?.content.push(child);
		}
	}
	return result;
}

/**
 * @param {{ heading: Heading, content: Array<RootContent> }} section
 * @returns {Feature}
 */
function parseFeature(section) {
		const name = mdastToString(section.heading)
		const isPackage = section.heading.depth <= 1;
		const tables = /** @type {Array<Table>} */ (section.content.filter(content => content.type === 'table'));
		const relations = tables.filter(isFeaturizerTable).flatMap(parseRelations);
		return { name: name, isPackage: isPackage, relations: relations };
}

/**
 * @param {Table} table 
 * @returns {boolean}
 */
function isFeaturizerTable(table) {
	if(table.children.length === 0) { return false; }
	const firstRow = table.children[0];
	if(firstRow.children.length === 0) { return false; }
	const firstCellText = mdastToString(firstRow.children[0]);
	if(firstCellText !== "Features" && firstCellText !== "Feature Usage") { return false; }
	return true;
}

/**
 * @param {Table} table
 * @returns {Array<Relation>}
 */
function parseRelations(table) {
	return table.children.slice(1).flatMap(row => {
		if(row.children.length < 2) { return []; }
		const firstCell = row.children[0];
		const secondCell = row.children[1];
		return secondCell.children
			.filter(child => child.type === 'linkReference')
			.map(link => ({ type: mdastToString(firstCell), feature: mdastToString(link) }));
	});
}

/**
 * @param {Array<Feature>} features 
 * @returns {Array<string>}
 */
export function checkFeatures(features) {
	const name2feature = new Map(features.map(feature => [feature.name, feature]));
	return features.flatMap(feature => checkFeature(feature, name2feature));
}

/**
 * @param {Feature} feature 
 * @param {Map<string, Feature>} name2feature 
 * @returns {Array<string>}
 */
function checkFeature(feature, name2feature) {
	return feature.relations.flatMap(relation => {
		const type = typeFromKeyword(relation.type);
		if(type === undefined) {
			return [
				'Unrecognized relation type "' + relation.type + '" in feature "' + feature.name + '".'
			];
		}
		const relatedFeature = name2feature.get(relation.feature);
		if(relatedFeature === undefined) {
			return [
				'Unknown feature "' + relation.feature + '" referenced by relation "' + relation.type + '" in feature "' + feature.name + '".'
			];
		}
		const oppositeType = opposite(type);
		const oppositeRelationPresent = relatedFeature.relations.some(r => r.type === oppositeType && r.feature === feature.name);
		if(!oppositeRelationPresent && !relatedFeature.isPackage && !feature.isPackage) {
			return [
				'Missing relation "' + oppositeType + '" to feature "' + feature.name + '" in feature "' + relatedFeature.name + '".'
			];
		}
		return [];
	});
}

checkReferenceDocs();
