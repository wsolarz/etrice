# Standalone Generators
The eTrice generators can be called from the command line. This is useful e.g. if they should participate in a build process driven by command line tools like make.

## Installation
1. Make sure that a Java Runtime Environment (JRE) is installed on your system.
The minimum required version is 11.
A JRE can be download from [Adoptium](https://adoptium.net/), for example.
Either the command `java` must be directly available on your `PATH` or the environment variable `JAVA_HOME` must be set to the location of the JRE.
2. The standalone generators are distributed as zip archive and can be downloaded from the [eTrice download page](https://eclipse.dev/etrice/downloads/).
Extract the archive and make the contained `bin` folder available on your `PATH`.

## Usage
Example usage for generating source code with the eTrice C generator:
```sh
etrice-c -genDir src-gen -modelpath model model/MyMapping.etmap model/MyModel.room
```
Source code is only generated for model files which are explicitly listed on the command line.
Make sure to enumerate all your model directories separated by path separators via the `-modelpath` argument so that references between models can be resolved properly.

To see all available command line options, run an eTrice generator with the `-help` argument.
