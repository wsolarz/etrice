# CodeGenerators


| Features |     |
| -------- | --- |
| Contains: | [CCodeGenerator][], [JavaCodeGenerator][], [CPPCodeGenerator][] |

## CCodeGenerator



| Features |     |
| -------- | --- |
| Contains: | [GenerationOptions][], [MSCLogging][] |



---


## CPPCodeGenerator






---


## GenerationOptions
Mechanism to adjust the generation.

Options for generation are configured in the launch configuration or in case of standalone generation via command line.
A list of available options:

- generate as library
- generate documentation
- generate instrumentation for MSC generation
- generate instrumentation for data logging
- override output directories
- debug options



| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [CCodeGenerator][], [JavaCodeGenerator][] |
| Is used by: | [MSCLogging][] |


---


## JavaCodeGenerator



| Features |     |
| -------- | --- |
| Contains: | [GenerationOptions][], [MSCLogging][] |



---


## MSCLogging
Runtime logger for event-driven Messages, represented as a Message Sequence Chart.

The MSCLogging is activated by default, but can be set manually in the [GenerationOptions][]. The output file is created upon regular termination of the application. The resulting file can be found in the logging directory and has the name *msc.seq*, which can be open with the free open source tool [Trace2UML](https://wiki.astade.de/).

![MSCLogging](../images/300-MSCLogging.png)


| Features |     |
| -------- | --- |
| Uses: | [GenerationOptions][] |

| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [CCodeGenerator][], [JavaCodeGenerator][] |


---


[CCodeGenerator]: #ccodegenerator
[JavaCodeGenerator]: #javacodegenerator
[CPPCodeGenerator]: #cppcodegenerator
[GenerationOptions]: #generationoptions
[MSCLogging]: #msclogging
