Tutorials
=========

Working with the tutorials
--------------------------

The eTrice tutorials will help you to learn and understand the eTrice tool and its concepts.
eTrice supports several target languages.

The Getting Started tutorials are target language specific.
The other tutorials apply for all target languages.
eTrice primarily supports C.
C++ generator and runtime are currently in prototype stage.
Java target language support is currently unmaintained.

-   Getting Started C - Reference template project

-   Ping Pong

-   Traffic Light (Example)

The tutorials are also available in their finished version and can be added to the workspace via the Eclipse New Wizard ( *File -&gt; New -&gt; Other... -&gt; eTrice -&gt; [language] -&gt; eTrice [language] Tutorials* ).

eTrice generates code out of ROOM models. The generated code relies on the services of a runtime framework (Runtime):

-   execution
-   communication (e.g. messaging)
-   logging
-   operating system abstraction (osal)


Additional functionality is provided as model library (Modellib):

-   socket server and client
-   timing service
-   standard types
