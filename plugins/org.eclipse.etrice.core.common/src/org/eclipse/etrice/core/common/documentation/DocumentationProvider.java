package org.eclipse.etrice.core.common.documentation;

import java.util.regex.Pattern;

import org.eclipse.xtext.documentation.impl.MultiLineCommentDocumentationProvider;

public class DocumentationProvider extends MultiLineCommentDocumentationProvider {
	
	@Override
	public void injectProperties(MultiLineCommentProviderProperties properties) {
		super.injectProperties(properties);
		
		// end tag is */ or **/
		this.endTagRegex = Pattern.compile("\\*\\*?/" + "\\z");
		// if line has no asterisk prefix, keep whitespace
		this.linePrefixRegex = Pattern.compile("(?m)^( |\\t)*\\* ?");
		// preserve line postfix, keep whitespace
		this.linePostfixRegex = Pattern.compile("\\z");
	}
}
