/*******************************************************************************
 * Copyright (c) 2022 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * CONTRIBUTORS:
 * 		epaen (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.support;

public class PosAndSize extends Pos {

	private int w, h;
	
	public PosAndSize(int x, int y, int w, int h) {
		super(x, y);
		this.w = w;
		this.h = h;
	}

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PosAndSize))
			return false;
		PosAndSize other = (PosAndSize)obj;
		
		return super.equals(other) && (this.w == other.w) && (this.h == other.h);
	}
}