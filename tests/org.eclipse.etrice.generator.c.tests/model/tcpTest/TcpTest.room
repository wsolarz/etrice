/**
 * Functional tests for etrice.api.tcp.ATcpServer/ATcpClient
 */
RoomModel TcpTest {

	import etrice.api.timer.ATimerService
	import etrice.api.timer.PTimer
	import etrice.api.types.*
	import etrice.api.tcp.ATcpClient
	import etrice.api.tcp.ATcpServer
	import etrice.api.tcp.PTcpControl
	import etrice.api.tcp.PTcpPayload
	import etrice.api.timer.targetTime
	import etrice.api.testcontrol.PTestControl
	import etrice.api.testcontrol.ControllableSequentialTestExecutor
	import etrice.api.tcp.DTcpControl

	Enumeration ETestEvent of int16 {
		SERVER_CONNECT,
		SERVER_DISCONNECT,
		SERVER_RECEIVE,
		CLIENT_CONNECT,
		CLIENT_DISCONNECT,
		CLIENT_RECEIVE,
		END
	}

	ActorClass ATcpClientWithConnectRetry {
		Interface {
			Port controlPort: PTcpControl
			Port payloadPort: PTcpPayload
		}
		Structure {
			external Port controlPort
			conjugated Port controlPort_internal: PTcpControl
			ActorRef client: ATcpClient
			Binding controlPort_internal and client.controlPort
			Binding payloadPort and client.payloadPort
			SAP timer: PTimer

			Attribute maxRetries: uint8 = "3"
			Attribute delayMs: uint16 = "1000"
			Attribute retries: uint8
			Attribute connectData: DTcpControl
		}
		Behavior {
			StateMachine {
				Transition init: initial -> ready
				Transition ctl_0: ready -> connecting {
					triggers {
						<connect: controlPort>
					}
					action '''
						connectData = *transitionData;
						controlPort_internal.connect(&connectData);
					'''
				}

				Transition tr0: connecting -> connecting {
					triggers {
						<disconnected: controlPort_internal> or <error: controlPort_internal>
					}
					action '''
						retries += 1;
						timer.startTimeout(delayMs);
						etLogger_logInfoF("tcpclientretry: unable to connect, retrying in %d ms (%d of %d)", delayMs, retries, maxRetries);
					'''
				}
				Transition tr1: connecting -> cp cp0 {
					triggers {
						<timeout: timer>
					}
				}
				Transition tr2: cp cp0 -> connecting {
					action '''controlPort_internal.connect(&connectData);'''
				}
				Transition tr3: cp cp0 -> ready {
					cond '''retries == maxRetries'''
					action '''
						controlPort.error();
						etLogger_logInfoF("tcpclientretry: max retries reached.", maxRetries);
					'''
				}
				Transition tr5: my ctl_handler -> my ctl_handler {
					triggers {
						<disconnect: controlPort>
					}
					action '''controlPort_internal.disconnect();'''
				}
				Transition tr4: my ctl_internal_global -> connected {
					triggers {
						<connected: controlPort_internal>
					}
					action '''controlPort.connected();'''
				}
				Transition tr6: my ctl_internal_global -> ready {
					triggers {
						<disconnected: controlPort_internal>
					}
					action '''controlPort.disconnected();'''
				}
				Transition tr7: my ctl_internal_handler -> my ctl_internal_handler {
					triggers {
						<error: controlPort_internal>
					}
					action '''controlPort.error();'''
				}
				State ready {
					entry '''
						timer.kill();
						retries = 0;
						connectData = (DTcpControl) {0};'''
				}
				State connecting
				State connected
				handler TransitionPoint ctl_handler
				TransitionPoint ctl_internal_global
				handler TransitionPoint ctl_internal_handler
				ChoicePoint cp0
			}
		}
	}

	/**
	 * Given an ATcpServer instance and multiple ATcpClient instances that are connected at the
	 * same time, when the clients send messages to the server, then the server is able to send
	 * replies to each individual client.
	 */
	ActorClass ASingleServerMultipleClientTest {
		Interface {
			Port testCtl: PTestControl
		}
		Structure {
			external Port testCtl
			conjugated Port serverCtl: PTcpControl
			conjugated Port serverData: PTcpPayload
			conjugated Port clientCtl[2]: PTcpControl
			conjugated Port clientData[2]: PTcpPayload

			ActorRef server: ATcpServer
			ActorRef client1: ATcpClientWithConnectRetry
			ActorRef client2: ATcpClientWithConnectRetry

			Binding server.controlPort and serverCtl
			Binding server.payloadPort and serverData
			Binding client1.controlPort and clientCtl
			Binding client1.payloadPort and clientData
			Binding client2.controlPort and clientCtl
			Binding client2.payloadPort and clientData

			Attribute numClients: uint8
			Attribute clientsConnected: uint8
			Attribute currentClient: int8
			Attribute serverAddress: DTcpControl
			Attribute testId: int16
		}
		Behavior {
			Operation setExpectations(events: ETestEvent ref, size: int16) '''
				EXPECT_ORDER_START(testId, events, size);
			'''
			Operation sendServerPayload(id: int16) '''
				DTcpPayload payload;
				DTcpPayload_setAsString(&payload, "server");
				payload.connectionId = id;
				serverData.dataPackage(&payload);
			'''
			Operation sendClientPayload() '''
				DTcpPayload payload;
				DTcpPayload_setAsString(&payload, "client");
				clientData[currentClient].dataPackage(&payload);
			'''
			StateMachine {
				Transition init: initial -> Pre {
					action '''
						numClients = 2;
						serverAddress.IPAddr = "127.0.0.1";
						serverAddress.TcpPort = 1234;
					'''
				}
				Transition start: Pre -> start of Running {
					triggers {
						<start: testCtl>
					}
					action '''
						testId = etUnit_openTestCase("TcpTest_SingleServerMultipleClient");
						static int16 resultlist[] = {
							ETestEvent.SERVER_CONNECT,
							ETestEvent.CLIENT_CONNECT,
							ETestEvent.CLIENT_CONNECT,
							ETestEvent.SERVER_RECEIVE,
							ETestEvent.CLIENT_RECEIVE, 0,
							ETestEvent.SERVER_RECEIVE,
							ETestEvent.CLIENT_RECEIVE, 1,
							ETestEvent.CLIENT_DISCONNECT,
							ETestEvent.CLIENT_DISCONNECT,
							ETestEvent.SERVER_DISCONNECT,
							ETestEvent.END,
						};
						setExpectations(resultlist, sizeof(resultlist) / sizeof(int16));
					'''
				}
				Transition done: done of Running -> Post {
					action '''
						testCtl.done(true);
					'''
				}
				Transition abort: Running -> Post {
					triggers {
						<abort: testCtl>
					}
					action '''
						EXPECT_TRUE(testId, "Aborted test", ET_FALSE);
						testCtl.done(false);
					'''
				}

				State Pre
				State Post {
					entry '''
						etUnit_closeTestCase(testId);
					'''
				}
				State Running {
					subgraph {
						EntryPoint start
						ExitPoint done
						State WaitServerConnect
						State WaitClientConnect
						State ClientSent {
							entry '''
								etLogger_logInfoF("tcptest: client %d sending payload", currentClient);
								sendClientPayload();
							'''
						}
						State ServerReceived
						State WaitClientDisconnect
						State WaitServerDisconnect

						// Connect
						/*Transition connectClient: my start -> WaitClientConnect {
							action '''
								//serverCtl.connect(&serverAddress);
								EXPECT_ORDER(testId, "server connect", ETestEvent.SERVER_CONNECT);
								clientCtl.connect(&serverAddress);
							'''
						}*/
						Transition connectServer: my start -> WaitServerConnect {
							action '''
								serverCtl.connect(&serverAddress);
							'''
						}
						Transition connectClient: WaitServerConnect -> WaitClientConnect {
							triggers {
								<connected: serverCtl>
							}
							action '''
								EXPECT_ORDER(testId, "server connect", ETestEvent.SERVER_CONNECT);
								clientCtl.connect(&serverAddress);
							'''
						}
						ChoicePoint checkClientsConnected
						Transition clientConnected: WaitClientConnect -> cp checkClientsConnected {
							triggers {
								<connected: clientCtl>
							}
							action '''
								EXPECT_ORDER(testId, "client connect", ETestEvent.CLIENT_CONNECT);
								etLogger_logInfoF("tcptest: client %d connected", <|ifitem.index|>);
								++clientsConnected;
							'''
						}
						Transition waitForConnections: cp checkClientsConnected -> WaitClientConnect
						Transition allConnected: cp checkClientsConnected -> ClientSent {
							cond '''clientsConnected == numClients'''
						}

						// Send and receive payloads
						ChoicePoint checkPayloadsReceived
						Transition serverPayload: ClientSent -> ServerReceived {
							triggers {
								<dataPackage: serverData>
							}
							action '''
								EXPECT_ORDER(testId, "server receive", ETestEvent.SERVER_RECEIVE);
								etLogger_logInfo("tcptest: server received payload, sending response");
								sendServerPayload(transitionData->connectionId);
							'''
						}
						Transition clientPayload: ServerReceived -> cp checkPayloadsReceived {
							triggers {
								<dataPackage: clientData>
							}
							action '''
								EXPECT_ORDER(testId, "client receive", ETestEvent.CLIENT_RECEIVE);
								EXPECT_ORDER(testId, "client receive idx", <|ifitem.index|>);
								etLogger_logInfoF("tcptest: client %d received payload", <|ifitem.index|>);
								++currentClient;
							'''
						}
						Transition sendNextClientPayload: cp checkPayloadsReceived -> ClientSent {
							action '''
								etLogger_logInfoF("tcptest: client %d sending payload", <|ifitem.index|>);
							'''
						}
						Transition disconnectClients: cp checkPayloadsReceived -> WaitClientDisconnect {
							cond '''currentClient == numClients'''
							action '''
								clientCtl.disconnect();
							'''
						}

						// Disconnect
						ChoicePoint checkClientsDisconnected
						Transition clientDisconnected: WaitClientDisconnect -> cp checkClientsDisconnected {
							triggers {
								<disconnected: clientCtl>
							}
							action '''
								EXPECT_ORDER(testId, "client receive", ETestEvent.CLIENT_DISCONNECT);
								etLogger_logInfoF("tcptest: client %d disconnected", <|ifitem.index|>);
								--clientsConnected;
							'''
						}
						Transition waitForDisconnects: cp checkClientsDisconnected -> WaitClientDisconnect
						Transition disconnectServer: cp checkClientsDisconnected -> WaitServerDisconnect {
							cond '''clientsConnected == 0'''
							action '''
								serverCtl.disconnect();
							'''
						}
						Transition trDone: WaitServerDisconnect -> my done {
							triggers {
								<disconnected: serverCtl>
							}
							action '''
								etLogger_logInfo("tcptest: server disconnected");
								EXPECT_ORDER(testId, "server disconnect", ETestEvent.SERVER_DISCONNECT);
								EXPECT_ORDER(testId, "end", ETestEvent.END);
							'''
						}
					}
				}
			}
		}
	}

	/**
	 * Given an ATcpServer instance and an ATcpClient instance that are connected and 
	 * can communicate with each other, when the client is disconnected and reconnected,
	 * then the server and client are still able to communicate.
	 */
	ActorClass ASingleServerSingleClientReconnectTest {
		Interface {
			Port testCtl: PTestControl
		}
		Structure {
			external Port testCtl
			conjugated Port serverCtl: PTcpControl
			conjugated Port serverData: PTcpPayload
			conjugated Port clientCtl: PTcpControl
			conjugated Port clientData: PTcpPayload

			ActorRef server: ATcpServer
			ActorRef client: ATcpClientWithConnectRetry

			Binding server.controlPort and serverCtl
			Binding server.payloadPort and serverData
			Binding client.controlPort and clientCtl
			Binding client.payloadPort and clientData

			Attribute serverAddress: DTcpControl
			Attribute testId: int16
			Attribute cycle: int16
		}
		Behavior {
			Operation setExpectations(events: ETestEvent ref, size: int16) '''
				EXPECT_ORDER_START(testId, events, size);
			'''
			Operation sendServerPayload(id: int16) '''
				DTcpPayload payload;
				DTcpPayload_setAsString(&payload, "server");
				payload.connectionId = id;
				serverData.dataPackage(&payload);
			'''
			Operation sendClientPayload() '''
				DTcpPayload payload;
				DTcpPayload_setAsString(&payload, "client");
				clientData.dataPackage(&payload);
			'''
			StateMachine {
				Transition init: initial -> Pre {
					action '''
						serverAddress.IPAddr = "127.0.0.1";
						serverAddress.TcpPort = 1234;
					'''
				}
				Transition start: Pre -> start of Running {
					triggers {
						<start: testCtl>
					}
					action '''
						testId = etUnit_openTestCase("TcpTest_SingleServerSingleClientReconnect");
						static int16 resultlist[] = {
							ETestEvent.SERVER_CONNECT,
							ETestEvent.CLIENT_CONNECT,
							ETestEvent.SERVER_RECEIVE,
							ETestEvent.CLIENT_RECEIVE,
							ETestEvent.CLIENT_DISCONNECT,
							ETestEvent.CLIENT_CONNECT,
							ETestEvent.SERVER_RECEIVE,
							ETestEvent.CLIENT_RECEIVE,
							ETestEvent.CLIENT_DISCONNECT,
							ETestEvent.SERVER_DISCONNECT,
							ETestEvent.END,
						};
						setExpectations(resultlist, sizeof(resultlist) / sizeof(int16));
					'''
				}
				Transition done: done of Running -> Post {
					action '''
						testCtl.done(true);
					'''
				}
				Transition abort: Running -> Post {
					triggers {
						<abort: testCtl>
					}
					action '''
						EXPECT_TRUE(testId, "Aborted test", ET_FALSE);
						testCtl.done(false);
					'''
				}

				State Pre
				State Post {
					entry '''
						etUnit_closeTestCase(testId);
					'''
				}
				State Running {
					subgraph {
						EntryPoint start
						ExitPoint done
						State WaitServerConnect
						State WaitClientConnect
						State ClientSent {
							entry '''
								sendClientPayload();
							'''
						}
						State ServerReceived
						State WaitClientDisconnect
						State WaitServerDisconnect

						Transition connectServer: my start -> WaitServerConnect {
							action '''
								serverCtl.connect(&serverAddress);
							'''
						}
						Transition connectClient: WaitServerConnect -> WaitClientConnect {
							triggers {
								<connected: serverCtl>
							}
							action '''
								EXPECT_ORDER(testId, "server connect", ETestEvent.SERVER_CONNECT);
								clientCtl.connect(&serverAddress);
							'''
						}
						Transition clientConnected: WaitClientConnect -> ClientSent {
							triggers {
								<connected: clientCtl>
							}
							action '''
								EXPECT_ORDER(testId, "client connect", ETestEvent.CLIENT_CONNECT);
							'''
						}
						Transition serverPayload: ClientSent -> ServerReceived {
							triggers {
								<dataPackage: serverData>
							}
							action '''
								EXPECT_ORDER(testId, "server receive", ETestEvent.SERVER_RECEIVE);
								sendServerPayload(transitionData->connectionId);
							'''
						}
						Transition clientPayload: ServerReceived -> WaitClientDisconnect {
							triggers {
								<dataPackage: clientData>
							}
							action '''
								EXPECT_ORDER(testId, "client receive", ETestEvent.CLIENT_RECEIVE);
								clientCtl.disconnect();
								++cycle;
							'''
						}

						ChoicePoint checkCyclesDone
						Transition clientDisconnected: WaitClientDisconnect -> cp checkCyclesDone {
							triggers {
								<disconnected: clientCtl>
							}
							action '''
								EXPECT_ORDER(testId, "client disconnect", ETestEvent.CLIENT_DISCONNECT);
							'''
						}
						Transition cyclesDone: cp checkCyclesDone -> WaitServerDisconnect {
							cond '''cycle == 2'''
							action '''
								serverCtl.disconnect();
							'''
						}
						Transition reconnect: cp checkCyclesDone -> WaitClientConnect {
							action '''
								clientCtl.connect(&serverAddress);
							'''
						}
						Transition trDone: WaitServerDisconnect -> my done {
							triggers {
								<disconnected: serverCtl>
							}
							action '''
								EXPECT_ORDER(testId, "server disconnect", ETestEvent.SERVER_DISCONNECT);
								EXPECT_ORDER(testId, "end", ETestEvent.END);
							'''
						}
					}
				}
			}
		}
	}

	ActorClass ATestHarness {
		Interface {
		}
		Structure {
			usercode2 '''
				#include <etUnit/etUnit.h>
				#include <osal/etSema.h>
				#include <runtime/etRuntime.h>
			'''
			conjugated Port runnerCtl: PTestControl
			SAP timeoutTimer: PTimer
			ActorRef runner: ControllableSequentialTestExecutor
			ActorRef t1: ASingleServerMultipleClientTest
			ActorRef t2: ASingleServerSingleClientReconnectTest
			Binding runner.control and t1.testCtl
			Binding runner.control and t2.testCtl
			Binding runner.exeControl and runnerCtl
		}
		Behavior {
			StateMachine {
				Transition init: initial -> Running {
					action '''
						etUnit_open("log", "TcpTest");
						etUnit_openTestSuite("org.eclipse.etrice.generator.c.tests.TcpTest");
						runnerCtl.start();
						timeoutTimer.startTimeout(30*1000);
					'''
				}
				State Running
				Transition done: Running -> Finished {
					triggers {
						<done: runnerCtl>
					}
					action '''
						etUnit_closeTestSuite();
						etUnit_close();
						etSema_wakeup(etRuntime_getTerminateSemaphore());
					'''
				}
				Transition abort: Running -> Running {
					triggers {
						<timeout: timeoutTimer>
					}
					action '''
						runnerCtl.abort();
					'''
				}
				State Finished
			}
		}
	}

	SubSystemClass TcpTestSubSys {
		LogicalThread defaultThread

		ActorRef tests: ATestHarness
		ActorRef timerService: ATimerService
		LayerConnection ref tests satisfied_by timerService.timer
	}
	
	LogicalSystem TcpTestLogSys {
		SubSystemRef main: TcpTestSubSys
	}
}