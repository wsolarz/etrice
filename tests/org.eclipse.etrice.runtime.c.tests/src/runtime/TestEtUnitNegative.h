/*******************************************************************************
 * Copyright (c) 2022 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * CONTRIBUTORS:
 * 		Eyrak Paen-Rochlitz (initial contribution)
 *
 *******************************************************************************/

#ifndef _TESTETUNITNEGATIVE_H_
#define _TESTETUNITNEGATIVE_H_

#include "etDatatypes.h"

void TestEtUnitNegative_runSuite(void);

#endif /* _TESTETUNITNEGATIVE_H_ */
